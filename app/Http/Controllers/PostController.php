<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
	public function index(Request $request, Post $post)
	{
		$allPosts = $post->whereIn(
			'user_id', 
			$request->user()->following()->pluck('users.id')->push($request->user()->id)
		)->with('user');
		$posts = $allPosts->orderBy('created_at', 'desc')
			->take($request->get('limit', 20))
			->get();

		return response()->json([
			'data' => [
				'posts' => $posts,
				'total' => $allPosts->count()
			]
		]);
	}
    public function store(Request $request)
    {
    	$post = $request->user()->posts()->create([
    		'body' => $request->body
    	]);
    	return response()->json($post->with('user')->find($post->id));

    }
}
