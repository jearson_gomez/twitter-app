@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" id="timeline">
        <div class="col-md-4">
            <form action="#" v-on:submit.prevent="postStatus">
                <div class="form-group">
                    <textarea 
                        class="form-control" 
                        rows="5" 
                        required
                        maxlength="140" 
                        placeholder="What are you upto?"
                        v-model="post"></textarea>
                </div>
                <button type="submit" class="btn btn-success form-control">Post</button>
            </form>
        </div>
        <div class="col-md-8">
            <div class="posts">
                <p v-if="!posts.length">No posts yet.</p>
                <div class="media" v-for="post in posts" :key="post.id" v-if="posts.length">
                    <img class="d-flex ml-3" :src="post.user.avatar">
                    <div class="media-body">
                        <div class="user">
                            <a :href="post.user.profile_url">
                                <strong>@{{ post.user.username }}</strong>
                            </a>
                            - @{{ post.human_created_at }}
                        </div>
                        <p>@{{ post.body }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
